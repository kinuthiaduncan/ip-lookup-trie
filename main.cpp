#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>
#include <utility>
#include <random>
#include <thread>

#include "trie.h"
#include "json.hpp"
#include "TextTable.h"
#include "main.h"

#define TEST_RUNS 10

using namespace std;
using json = nlohmann::json;

typedef trie::trie_map<char, int> IP_TABLE;
IP_TABLE route_table;

json read_data(string path);//forward declarations
void create_table(json master_data);

multimap<string, pair<bool, double>> run_sequential_tests(json);

multimap<string, pair<bool, double>> run_parallel_tests(json);

//void* run_parallel_tests(json);
void comp_metrics(multimap<string, pair<bool, double>>);

template<typename T, typename RandomGenerator>
T select_randomly(T start, T end, RandomGenerator &g);

template<typename T>
T select_randomly(T, T);

int main() {
    //init
    json master_data = read_data("master_table.json");
    json test_data = read_data("test_ips.json");
    create_table(master_data);

    //run tests

    //multimap<string,pair<bool,double>> seq_results=run_sequential_tests(test_data);
    multimap<string, pair<bool, double>> seq_results = run_parallel_tests(test_data);

    comp_metrics(seq_results);
}

json read_data(string path) {
    ifstream ifs(path);
    json j = json::parse(ifs);
    return j;
}

void create_table(json j) {
    for (json::iterator it = j["table"].begin(); it != j["table"].end(); ++it) {
        string ip = it.value()["ip"];
        string port = it.value()["port"];
        route_table.insert(ip, stoi(port));
    }
}

multimap<string, pair<bool, double>> run_sequential_tests(json test_data) {
    multimap<string, pair<bool, double>> results;
    for (int j = 0; j < TEST_RUNS; j++) {
        auto selected_data = *select_randomly(test_data["test"].begin(), test_data["test"].end());
        string ip = selected_data["ip"];

        auto start = chrono::high_resolution_clock::now();
        bool found = route_table.contains(ip);
        auto finish = chrono::high_resolution_clock::now();
        chrono::duration<double> elapsed = finish - start;
        pair<bool, double> temp(found, chrono::duration_cast<chrono::nanoseconds>(elapsed).count());
        results.insert(make_pair(ip, temp));
    }
    return results;
}

multimap<string, pair<bool, double>> run_parallel_tests(json test_data) {

    multimap<string, pair<bool, double>> results;

    parallel_for(TEST_RUNS, [&](int start, int end) {
        for (int i = start; i < end; ++i) {
            auto selected_data = *select_randomly(test_data["test"].begin(), test_data["test"].end());
            string ip = selected_data["ip"];
            auto start = chrono::high_resolution_clock::now();
            bool found = route_table.contains(ip);
            auto finish = chrono::high_resolution_clock::now();
            chrono::duration<double> elapsed = finish - start;
            pair<bool, double> temp(found, chrono::duration_cast<chrono::nanoseconds>(elapsed).count());
            results.insert(make_pair(ip, temp));
        }
    });

    return results;
}

void comp_metrics(multimap<string, pair<bool, double>> seq_results) {
    TextTable t('-', '|', '+');
    t.add("test ip_addr");
    t.add("ip_addr found");
    t.add("test run_time (ns)");
    t.endOfRow();
    for (auto it = seq_results.cbegin(); it != seq_results.cend(); ++it) {
        t.add(it->first);
        t.add(to_string(it->second.first));
        t.add(to_string(it->second.second));
        t.endOfRow();
    }
    t.setAlignment(2, TextTable::Alignment::RIGHT);
    cout << t;
}

template<typename T, typename RandomGenerator>
T select_randomly(T start, T end, RandomGenerator &g) {
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

template<typename T>
T select_randomly(T start, T end) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return select_randomly(start, end, gen);
}